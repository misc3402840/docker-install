# Docker installation

## Linux

### Install

[Docker docs - Install docker engine on deian-like distro](https://docs.docker.com/engine/install/debian/#install-using-the-repository)

```bash
apt update && \
    apt install ca-certificates curl gnupg -y && \
    install -m 0755 -d /etc/apt/keyrings && \
    curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg && \
    chmod a+r /etc/apt/keyrings/docker.gpg && \
    echo "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null && \
    apt update && \
    apt install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin -y && \
	docker run hello-world
```

### Post-install

Create docker network for reverse proxy:

```bash
docker network create frontend
```

Update docker builder anf configure remote access for Docker daemon. Edit file: 

```bash
nano /etc/docker/daemon.json
``````


```bash
{
  "hosts": ["unix:///var/run/docker.sock", "tcp://0.0.0.0:2375"],
  "features": {
    "buildkit": true
  }
}
```

Edit docker service:

```bash
systemctl edit docker
```

with:

```conf
[Service]
ExecStart=
ExecStart=/usr/bin/dockerd --containerd=/run/containerd/containerd.sock
```

[Docker docs - BuildKit](https://docs.docker.com/build/buildkit/)

[Docker docs - remote acces](https://docs.docker.com/config/daemon/remote-access/)

Restart services:

```bash
systemctl daemon-reload && \
systemctl stop docker && \
systemctl start docker
```

### Uninstall

[Docker docs - Uninstall docker engine](https://docs.docker.com/engine/install/debian/#uninstall-docker-engine)

```bash
apt purge docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin docker-ce-rootless-extras && \
rm -rf /var/lib/docker && \
rm -rf /var/lib/containerd
